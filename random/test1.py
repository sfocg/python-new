#!/usr/bin/env python3


from netaddr import IPNetwork
import subprocess

for ip in IPNetwork('1.1.1.1/19'):
    subprocess.call(["host", str(ip)])

