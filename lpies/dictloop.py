#!/usr/bin/env python3

'''
Script that shows you how to iterate over lists, tuples and dictionaries
as well as enumberate the positional numbers of lists
'''


# Declare variables

chars = ['A', 'B', 'C']
fruit = ('Apple', 'Banana', 'Cherry')
dict = {'name' : 'Mike', 'ref' : 'Python', 'sys' : 'Windows'}

# Print the elements in a list

print ('\nElements:\t', end = ' ')
for item in chars:
    print (item, end=' ')

# Print the items in a list and enumerate their position in the list with enumerate

print ('\nEnumerated:\t', end = '')
for item in enumerate(chars): 
    print (item, end=' ')

# Print Tuple and List elemets with Zip

print ('\nZipped:\t', end=' ')
for item in zip (chars, fruit):
    print (item, end = ' ')

# Print Dictionary keys and values

print ('\nPaired:')
for key , value in dict.items():
    print (key, '=', value)





print ('\n')


