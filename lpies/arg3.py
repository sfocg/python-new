#!/usr/bin/env python3

def echo(user, lang, sys):
    print('User: ', user, 'Language: ', lang, 'Platform: ', sys)

echo('Mike', 'Python', 'Windows')
