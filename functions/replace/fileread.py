#!/usr/bin/env python3


def readfile(filename):

    '''
    Function to open a file, format it and read it into
    a variable called filename
    '''

    fobj = open(filename)
    for line in fobj:
        print(line.rstrip())
    return filename
    return fobj
    fobj.close()

def readnewfile(filename, torep, withrep):

    '''
    Function to replace strings in file - NOT USED
    '''

    fobj = open(filename)
    for line in fobj:
        print(line.strip('\n'))
        print (fobj.replace(torep, withrep))
    return filename
    fobj.close()
