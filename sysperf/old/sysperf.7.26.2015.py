#!/usr/bin/env python3


'''

This script is a system information and performance information
gathering script. It will pull information regarding live stats
like memory and cpu as well as information like os version and
serial number.

It's designed to be cross platform between Windows and OSX but
some data just isn't available on both.

This is an informational script only. It is not designed to change
any information, although with a few tweaks it could be.

In the spirit of being easy to run, I've only applied funtions that
don't require root/admin priviledges to run, so that any average
user/process can use and utilize this.

I may eventually branch this and remove psutil, as it's not a standard
module and requires some work to install. I would like this script
to be runnable from a default python install across platforms, so
I may eventually completely isolate the OSX and windows functions
and remove the cross platform section, putting in some logic to make
it transparent to the user.

Chris Gleason

Last update - 7/12/2015

Written for Python 3.x

### NOTES: ###

FUTURE WORK

1) Figure out if dependencies exist and if not exit properly with an informative message
2) If you can elegantly install the dependencies.
3) Need to run as sudo to pull network info from psutil

DEPENDENCIES

psutil

OSX

wget https://pypi.python.org/packages/source/p/psutil/psutil-3.1.0.tar.gz /tmp
tar -zxvf /tmp/psutil-3.1.0.tar.gz /tmp/
pip install /tmp/psutil-3.1.0/psutil

WINDOWS

https://pypi.python.org/packages/3.4/p/psutil/psutil-3.1.0.win32-py3.4.exe#md5=eb4504f7da8493a512a6f038a502a25c


'''

__version__ = "$Revision: 1"

################
# IMPORTS HERE #
################

import subprocess
import os
import platform
import sys
import argparse
import psutil
import readline
import time



###########################################
# ARGUMENTS AND SCRIPT RELATED ITEMS HERE #
###########################################


parser = argparse.ArgumentParser(description='Print system usage statistics and system information. \
    Default (no args) will determine OS and gather ALL information')
parser.add_argument('--ntfs' , 
    action='store_true' ,
    help='Gather Windows information')
parser.add_argument('--osx' , 
    action='store_true' ,
    help='Gather OSX information')
parser.add_argument('--sys' , 
    action='store_true' ,
    help='Gather System Information')
parser.add_argument('--perf' , 
    action='store_true' ,
    help='Gather Performance Information')

args = parser.parse_args()


##############################################
# NON PLATFORM SPECIFIC FUNCTIONS START HERE #
##############################################

def noargs():

    '''
    This function is to determine OS version if the user doesn't define it in the
    command line switches
    '''

    if os == 'ntfs':
        print('Platform is NTFS/Windows!')
        runperf()
        runsys()
        runntfs()
    else:
        print('Platform is OSX!')
        runperf()
        runsys()
        runosx()




def runperf():

    '''
    This function runs cross platform performance related tests
    '''

    title = 'PERFORMANCE TESTS RUNNING!'
    titlelen = len(title)
    
    print('#' * titlelen)
    print(title)
    print('#' * titlelen)


    print ('''
    ''')
    print('--------')
    print('CPU INFO')
    print('--------')
    print('')
    print('CPU times at runtime are ', psutil.cpu_times())
    print('')
    print('CPU percent per CPU at runtime is ', psutil.cpu_percent(interval=5, percpu=True))
    print('')
    print('''
    ''')
    print('-----------')
    print('MEMORY INFO')
    print('-----------')
    print('')
    print('Memory usage statistics are ', psutil.virtual_memory())
    print('')
    print('''
    ''')
    print('---------')
    print('DISK INFO')
    print('---------')
    print('')
    if sys.platform == 'darwin':
        print('Disk usage is:\n')
        print(subprocess.call(['/bin/df', '-h']))
        print('')
        #print('Space usage from root down is:\n', subprocess.call(['/usr/bin/du', '-hs', '/*',]))
        print('')
        print('Disk IO statistics are\n ')
        print(subprocess.call(['/usr/sbin/iostat', '-c10']))
        print('')
        print('Be sure to ignore the first iostat line, as per best practices')
        print('')
    if sys.platform == 'win32':
        print('Disk usage is ', )
 
    print('')
    print('''
    ''')
    print('------------')
    print('NETWORK INFO')
    print('------------')
    print('')
    print('Network I/O stats ', psutil.net_io_counters(pernic=False))
    print('')
    print('')
    print('')
    print('')



def runsys():

    '''
    This function runs cross platform system information gathering
    '''

    title = 'SYSTEM INFORMATION GATHERING!'
    titlelen = len(title)
    
    print('#' * titlelen)
    print(title)
    print('#' * titlelen)


    print ('''

    ''')
    print('Your OS is ', platform.system(), platform.release(), '-', platform.version())
    print('')
    print('Your architecture is ', platform.architecture())
    print('')
    print('# of logical CPU\'s are ', psutil.cpu_count())
    print('')
    print('# of physical CPU\'s, including threaded are ', psutil.cpu_count(logical=False))
    print('')
    print('Disk information is ', psutil.disk_partitions(all=True))
    print('') 
    if os == 'osx':
        print('Users on the system are:\n')
        print(subprocess.call(['who', '-a']))
    print('')
    if os == 'ntfs':
        print('Users on the system are:\n')
    print('')
 

######################################
# WINDOWS SPECIFIC THINGS START HERE #
######################################

def runntfs():

    '''
    This function runs the Windows specific gatherers
    '''

    #print('NTFS Tests running!')

    title = 'NTFS TESTS RUNNING!'
    titlelen = len(title)
    
    print('#' * titlelen)
    print(title)
    print('#' * titlelen)



############################
# OSX SPECIFIC THINGS HERE #
############################


def runosx():

    '''
    This function runs the OSX specific gatherers
    '''


    title = 'OSX SPECIFIC INFO GATHERING!'
    titlelen = len(title)
    
    print('#' * titlelen)
    print(title)
    print('#' * titlelen)

    print('')
    print('SCUTIL HOSTNAME set to ')
    print(subprocess.call(['/usr/bin/env', 'scutil', '--get', 'HostName']))
    print('SCUTIL LOCALHOSTNAME set to')
    print(subprocess.call(['/usr/bin/env', 'scutil', '--get', 'LocalHostName']))
    print('SCUTIL COMPUTERNAME set to')
    print(subprocess.call(['/usr/bin/env', 'scutil', '--get', 'ComputerName']))
    print()
    print('---------------')
    print('System Defaults')
    print('---------------')
    print()
    print('System Resume is disabled or not (0 = False and 1 = True)') 
    print('defaults read com.apple.systempreferences NSQuitAlwaysKeepsWindows -bool')
    #print(resume)
    FNULL = open(os.devnull, 'w')
    resume = subprocess.call(['defaults', 'read', 'com.apple.systempreferences', 'NSQuitAlwaysKeepsWindows', '-bool'], stderr=FNULL)
    print()
    

#################
# MAIN CODE RUN #
#################


if __name__ == "__main__":
    if sys.platform == 'win32':
        os == 'ntfs'
    elif sys.platform == 'darwin':
        os == 'osx'

    if args.ntfs and args.osx:
        print ("You can't run both Windows and OSX flags on the same system!")
        exit(0)

    if args.ntfs:
        print('You chose NTFS!')
        runntfs()
    elif args.osx:
        print('You chose OSX!')
        runosx()
    else:
        print('No OS specified!')


    if args.sys and args.perf:
        print('You chose both System and Performance tests!')
        runperf()
        runsys()
    elif args.sys:
        print('You chose to run System Information gathering only!')
        runsys()
    elif args.perf:
        print('You chose to tun Performance Metric tests only!')
        runperf()
    else:
        print("You didn't specify performance or system so both will be run!")


    #if len(args) == 0:
    #    noargs()

    if not len(sys.argv) > 1:
        noargs()












